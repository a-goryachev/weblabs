function getUrlInfo(url) {
    var match = url.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)(\/[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
	protocol: match[1],
	host: match[2],
	hostname: match[3],
	port: match[4],
	pathname: match[5],
	search: match[6],
	hash: match[7]
    }
}

function cookiesBundle() {
    this.cookies = [];

    this.add = function (cookie) {
	this.cookies.push(cookie);
    };

    this.update = function (items) {
	if (!items) {
	    return;
	}

	for (var i = 0; i < items.length; i++) {
	    var found = false;
	    for (var j = 0; j < this.cookies.length; j++) {
		if (this.cookies[j].name == items[i].name
			&& this.cookies[j].path == items[i].path
			&& this.cookies[j].domain == items[i].domain) {
		    this.cookies[j] = jQuery.extend({}, items[i]);
		    found = true;
		    break;
		}
	    }
	    if (!found) {
		this.cookies.push(jQuery.extend({}, items[i]));
	    }
	}
    };

    this.remove = function (ind) {
	this.cookies.splice(ind);
    };

    this.removeExpired = function () {
	var i;
	console.log("removing expired cookies...")
	for (i = 0; i < this.cookies.length; i++) {
	    var expire = this.cookies[i].expires;
	    if (expire) {
		var curDate = new Date();
		var expireDate = new Date(Date.parse(expire));
		if (expireDate.getTime() < curDate.getTime()) {
		    this.cookies[i] = null;
		}
	    }
	}
	for (i = 0; i < this.cookies.length; i++) {
	    if (this.cookies[i] === null) {
		this.cookies.splice(i);
		i--;
	    }
	}
    };

    this.getByUrl = function (url) {
	var domain = getUrlInfo(url).hostname;
	if (!domain) {
	    return this.getAll();
	}
	this.removeExpired();
	var result = [];
	/* фильтр по домену */
	var domainCookies = this.getByDomain(domain);
	for (var i = 0; i < domainCookies.length; i++) {
	    var expires = this.cookies[domainCookies[i]].expires;
	    if (expires) {
		/* проверим срок жизни */

	    }
	    result.push(this.cookies[domainCookies[i]]);
	}
	/* фильтр по expires */


	return result;
    };

    this.getByDomain = function (domain) {
	var result = [];
	/* фильтр по домену */
	for (var i = 0; i < this.cookies.length; i++) {
	    var cookie = this.cookies[i];
	    if (cookie.domain) {
		if (cookie.domain === domain) {
		    result.push(i);
		    continue;
		}

		if (/^\./.test(cookie.domain) === false && domain.indexOf(cookie.domain, domain.length - cookie.domain.length) !== -1) {
		    /* Домен из куки без ведущей точки и имя хоста оканчивается 
		     * на домен из куки
		     */
		    result.push(i);
		    continue;
		}
		if (/^\./.test(cookie.domain) && domain.indexOf(cookie.domain, domain.length - cookie.domain.length) !== -1) {
		    /* Домен из куки с ведущей точкой и имя хоста оканчивается 
		     * на домен из куки
		     */
		    result.push(i);
		    continue;
		}
		if (/^\./.test(cookie.domain) && domain.indexOf(cookie.domain.substr(1), domain.length - cookie.domain.substr(1).length) !== -1) {
		    /* Домен из куки с ведущей точкой и имя хоста оканчивается
		     * на домен из куки без учёта ведущей точки
		     */
		    result.push(i);
		    continue;
		}
		if (/^\*\./.test(cookie.domain) && domain.indexOf(cookie.domain.substr(1), domain.length - cookie.domain.substr(1).length) !== -1) {
		    /* Домен из куки с ведущей "*." и имя хоста - это поддомен
		     */
		    result.push(i);
		    continue;
		}
	    }
	}
	return result;
    };

    this.getAll = function () {
	return this.cookies;
    };
}

function tabsData() {
    this.data = {};
    this.save = function () {
	chrome.storage.local.set({'tabsData': this.data}, function () {
//	    console.log('Tabs data saved to storage.');
	});
    };

    this.load = function () {
	chrome.storage.local.get('tabsData', jQuery.proxy(function (items) {
	    this.data = items.tabsData || {};
//	    console.log('Tabs data loaded from storage.');
	}, this));
    };

    this.get = function (tabId) {
//	console.log("get\r\n" + jQuery.toJSON(this.data));
	return this.data['' + tabId] || this.defaultValue();
    };

    this.set = function (tabId, data) {
//	console.log("set\r\n" + jQuery.toJSON(this.data) + "\r\n" + jQuery.toJSON(data));
	this.data['' + tabId] = data;
//	this.save();
    };

    this.defaultValue = function () {
	return jQuery.extend({}, {
	    virtual: 0,
	    sidebarState: extensionOptions.sidebarState || 0,
	    cookies: new cookiesBundle()
	});
    };

}

var tabsData = new tabsData();

//tabsData.load();

var extensionOptions = {};

console.log('Background.html starting!');
/*Put page action icon on all tabs*/
chrome.tabs.onUpdated.addListener(function (tabId) {
    chrome.pageAction.show(tabId);
});

chrome.tabs.getSelected(null, function (tab) {
    chrome.pageAction.show(tab.id);
});

/*Send request to current tab when page action is clicked*/
chrome.pageAction.onClicked.addListener(function (tab) {
    chrome.tabs.getSelected(null, function (tab) {
	console.log('tabid: ' + tab.id);
	chrome.tabs.sendRequest(
		//Selected tab id
		tab.id,
		//Params inside a object data
			{callFunction: "toggleSidebar"},
		//Optional callback function
		function (response) {
		    console.log(response);
		}
		);
	    });
});
console.log('Background.html done.');

reloadOptions();

function reloadOptions() {
    chrome.storage.sync.get({
	sidebarState: 0,
	servername: 'localhost'
    }, function (options) {
	extensionOptions = options;
	setProxyServer(options.servername);
    });
}
function setProxyServer(servername) {
    var config = {
	mode: "fixed_servers",
	rules: {
	    proxyForHttp: {
		scheme: "socks5",
		host: servername,
		port: 9050
	    },
	    proxyForHttps: {
		scheme: "socks5",
		host: servername,
		port: 9050
	    },
	    bypassList: []
	}
    };
    chrome.proxy.settings.set(
	    {value: config, scope: 'regular'},
    function () {
    });
}

chrome.runtime.onMessage.addListener(
	function (request, sender, sendResponse) {
	    if (sender.tab) {
		if (request.type) {
		    var data = tabsData.get(sender.tab.id);
		    switch (true) {
			case request.type === 'reloadOptions':
			    reloadOptions();
			    break;
			case request.type === 'changeVirtual':
			    data.virtual = request.virtualId;
			    tabsData.set(sender.tab.id, data);
			    sendResponse({result: true});
			    break;
			case request.type === 'setSidebarState':
			    data.sidebarState = request.state;
			    tabsData.set(sender.tab.id, data);
			    break;
			case request.type === 'getSidebarState':
			    sendResponse({state: data.sidebarState});
			    break;
			case request.type === 'getVirtualId':
			    sendResponse({result: data.virtual});
			    break;
		    }

		}
	    }
	});

chrome.webRequest.onBeforeSendHeaders.addListener(
	function (request) {
	    var i, j, k;
	    var data = tabsData.get(request.tabId);

	    var requestDomain = '';
	    if (request.url) {
		requestDomain = getUrlInfo(request.url).hostname;
	    }
	    console.log('send request to url: ' + request.url);
	    console.log('domain: ' + requestDomain);

	    var localCookies = data.cookies.getByUrl(request.url);
	    console.log("stored cookies:\r\n" + jQuery.toJSON(localCookies));
	    if (data.virtual !== 0) {
		console.log('setting x-virtual header to ' + data.virtual);
		request.requestHeaders.push({name: 'x-virt', value: "" + data.virtual});
	    }
	    var cookies = [];
	    var cookiesHeaderIndex = -1;
	    for (i = 0; i < request.requestHeaders.length; i++) {
		if (/^cookie/i.test(request.requestHeaders[i].name)) {
		    cookiesHeaderIndex = i;
		    cookies = request.requestHeaders[i].value.split('; '); //все Cookies передаются одним заголовком
		}
	    }
	    /**
	     * @todo
	     * 1. Брать хранимые куки по домену из URL запроса
	     * 2. Фильтровать по path
	     * 3. Фильтровать по expires
	     * 4. Фильтровать по secure
	     * 5. Оправлять на ресурс
	     * 6. Куки, которые отправил браузер (созданные JS скриптом)
	     * пропускать как есть.
	     */
	    if (cookiesHeaderIndex > -1) {

		/* Подменим передаваемые куки сохранёнными куками виртауала.
		 * Вообще этого не должно быть т.к. пришедших из сети куков 
		 * в браузере не будет (если включено расширение), 
		 * но мало ли. Вдруг выключили и сходили куда-то.
		 */
		var ignoreLocalCookies = [];
		for (j = 0; j < cookies.length; j++) {
		    cookies[j] = cookies[j].split('=');
		    for (k = 0; k < localCookies.length; k++) {
			if (localCookies[k].name === cookies[j][0]) {	//подменяем
			    cookies[j][1] = localCookies[k].value;
			    ignoreLocalCookies.push(k);
			    break;
			}
		    }
		    cookies[j] = cookies[j].join('=');
		}
		/* Удалим хранимые куки, которые уже использовали для подмены */
		for (j = 0; j < ignoreLocalCookies.length; j++) {
		    localCookies.splice(j);
		}
	    }
	    /* Добавим куки, хранящиеся в профиле */
	    for (j = 0; j < localCookies.length; j++) {
		cookies.push("" + localCookies[j].name + "=" + localCookies[j].value);
	    }

	    if (cookies.length > 0) {
		if (cookiesHeaderIndex > -1) {
		    request.requestHeaders[cookiesHeaderIndex].value = cookies.join('; '); //ставим новые cookies
		} else {
		    request.requestHeaders.push({name: "Cookie", value: cookies.join('; ')});
		}
	    } else {
		if (cookiesHeaderIndex > -1) {
		    request.requestHeaders.splice(cookiesHeaderIndex);
		}
	    }

	    console.log(jQuery.toJSON(request.requestHeaders))

	    return {requestHeaders: request.requestHeaders};
	},
	{urls: []},
['requestHeaders', 'blocking']
	);

chrome.webRequest.onBeforeRequest.addListener(
	function (request) {
	},
	{urls: []},
['requestBody', 'blocking']
	);

chrome.webRequest.onHeadersReceived.addListener(
	function (request) {
	    var urlHostname = getUrlInfo(request.url).hostname;

	    if (request.tabId) {
		var data = tabsData.get(request.tabId);
		//Cookies устанавливаются при получении заголовка следующего вида:
		//name: "Set-Cookie"
		//value: "cookiename=value; expires=Sun, 22-Dec-2013 03:31:23 GMT; path=/; domain=.domain.com"
		console.log('headers received for url: ' + request.url);
		for (var i = 0; i < request.responseHeaders.length; i++) {
		    if (/^set\-cookie/i.test(request.responseHeaders[i].name)) {
			console.log(jQuery.toJSON(request.responseHeaders[i]));
			/**
			 * @todo
			 * 1. Парсить все части куки
			 * 2. Записывать куку в профиль виртуала
			 * 3. Удалять куку из профиля, если истёк expires или 
			 * value=""
			 * 4. Не пропускать куку в браузер
			 */

			var cookieData = request.responseHeaders[i].value.split('; ');
			cookieData[0] = cookieData[0].split('=');
			var cookie = {name: cookieData[0][0], value: cookieData[0][1]};
			for (var j = 1; j < cookieData.length; j++) {
			    cookieData[j] = cookieData[j].split('=');
			    cookie[cookieData[j][0]] = cookieData[j][1];
			}

			if (!cookie.domain) {
			    cookie.domain = urlHostname;
			}
			console.log("received cookie:\r\n" + jQuery.toJSON(cookie));

			var cookieFound = false;
			var localCookies = data.cookies.getByUrl(request.url);
			console.log("stored cookies:\r\n" + jQuery.toJSON(localCookies));
			for (var k = 0; k < localCookies.length; k++) {
			    if (localCookies[k].name === cookie.name && localCookies[k].path === cookie.path && localCookies[k].domain === cookie.domain) {
				//Есть такой хранимый cookie, надо обновить
				console.log('update stored cookie ' + cookie.name);
				localCookies[k].value = cookie.value;
				cookieFound = true;
				break;
			    }
			}
			if (!cookieFound) {
			    //Запишем cookie
			    localCookies.push(cookie);
			}
			data.cookies.update(localCookies);
			tabsData.set(request.tabId, data);
			console.log('virtual cookies updated');
			console.log(jQuery.toJSON(data));
//		    браузеру это возвращать не нужно, иначе он у себя заменит cookies текущей реальной сессии, мы же держим отдельную сессию только для конкретной вкладки
			request.responseHeaders.splice(i, 1);
			i--;
		    }
		}

	    }
	    return {responseHeaders: request.responseHeaders};
	}, {
    urls: ["<all_urls>"]
}, [
    "blocking",
    "responseHeaders"
]);

/* События вкладок */
chrome.tabs.onCreated.addListener(function (tab) {
    if (tab.openerTabId && tab.id) {
	console.log("opener: " + tab.openerTabId);
	/* подставляем виртуала */
	var data = tabsData.get(tab.openerTabId);
	tabsData.set(tab.id, jQuery.extend({}, data));
    }
});
chrome.tabs.onUpdated.addListener(function (tabId, changeInfotab) {
});

/* Cookies */

chrome.cookies.onChanged.addListener(function (info) {
    //console.log("Cookie onChanged:" + JSON.stringify(info));
});