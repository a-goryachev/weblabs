<?php

namespace console\controllers;

use yii\helpers\FileHelper;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Stream;
use Prewk\XmlStringStreamer\Parser;
use common\models\Constant;

/**
 * ���������� ��������� ������� �� ���.
 *
 * @author Alexander Goryachev
 */
class DetbalWorkerController extends \console\base\Worker
{

    const FILE_STATUS_BILLING = 'b';
    const FILE_STATUS_BILLPAGE = 'p';
    const FILE_STATUS_TALKS = 't';
    const FILE_STATUS_DONE = 'd';

    public $defaultAction = "process";
    private $numberIds = [];
    private $billingData = [];
    private $cacheNumber = [];

    /**
     * ��������� ���������� ������
     */
    public function actionProcess()
    {
        $markedFiles = [];
        foreach ($this->getFiles() as $file) {
            $newFile = $this->markFile($file, self::FILE_STATUS_BILLING);
            if ($newFile !== false) {
                $markedFiles[] = $newFile;
            }
        }

        /* ���������� � ������� � ��� ������ ���������� */
        foreach ($markedFiles as $file) {
            try {
                /* ������ ���� � ������� ��������� */
                $fileName = $file;
                while (!file_exists($fileName)) {
                    $status = substr($fileName, -1, 1);
                    if ($status == self::FILE_STATUS_BILLING) {
                        $fileName.='.' . self::FILE_STATUS_BILLPAGE;
                        continue;
                    } elseif ($status == self::FILE_STATUS_BILLPAGE) {
                        $fileName.='.' . self::FILE_STATUS_TALKS;
                        continue;
                    } else {
                        break;
                    }
                }
                $this->loadTalksData($fileName);
            } catch (exceptions\DetbalFileNotFoundException $e) {
                /* �� ����� ���� */
                \Yii::error($e->getMessage(), 'detbal-worker');
            } catch (\Exception $e) {
                \Yii::error($e->getMessage(), 'detbal-worker');
                \Yii::trace($e->getTrace(), 'detbal-worker');
            }
        }
    }

    /**
     * ������ ��������� ������� ������� ���������
     */
    public function actionBillpages()
    {
        foreach ($this->getFiles(self::FILE_STATUS_BILLPAGE) as $file) {
            $this->updateBillPages($file);
            $this->markFile($file, self::FILE_STATUS_TALKS);
        }
    }

    /**
     * ������ ��������� ����������� ���������� ���������
     */
    public function actionTalks()
    {
        $filesToWork = $this->getFiles(self::FILE_STATUS_TALKS);
        $filesWithCachedData = [];

        foreach ($filesToWork as $file) {
            try {
                $this->loadTalksData($file, true);
                $filesWithCachedData[] = $file;
            } catch (exceptions\CachedDataNotAvailableException $ignore) {
                /* ������ ��� � ���� */
                echo "Cached data not available for file {$file}.\n";
            } catch (\Exception $e) {
                \Yii::error($e->getMessage(), 'detbal-worker');
            }
        }

        $files = $filesWithCachedData === [] ? $filesToWork : $filesWithCachedData;
        foreach ($files as $file) {
            try {
                $this->updateTalks($file);
                $this->markFile($file, self::FILE_STATUS_DONE);
            } catch (\Exception $e) {
                \Yii::error($e->getMessage(), 'detbal-worker');
            }
        }
    }

    /**
     * ����� ������� ����� ������������ ������
     */
    public function actionUndone()
    {
        foreach ($this->getFiles(self::FILE_STATUS_DONE) as $file) {
            rename($file, preg_replace('#^(.*\.xml).*\.' . self::FILE_STATUS_DONE . '$#', "\\1", $file));
        }
    }

    /**
     * ������ ��������� �������� ���������
     */
    public function actionBilling()
    {
        $filesToWork = $this->getFiles(self::FILE_STATUS_BILLING);
        foreach ($filesToWork as $file) {

            $detbalData = $this->getDetbalData($file);

            if ($detbalData === false) {
                \Yii::error("�� ������� �������� ������ �������� ����� �� ����� ���������� ������ {$file}.");
                continue;
            }

            $bstamp = $detbalData['ed'] . ' 23:59:59';

            $account = $this->getAccount($detbalData['an']);

            $operation = $this->createOperation($account);

            $processedNumbers = $this->updateBilling($file, $account, $operation);
            $this->updateCommission($file, $account, $operation);

            /* ��������� ����� ������� �������� ������� */
            foreach ($this->loadBillingData($file) as $data) {
                $number = $this->getNumber(substr($data['number'], -10), $account);
                $number->lastbillingtime = $bstamp;
                $number->save();
            }
            /* ��������� ����� ������� �������� �������� ����� */
            if (strtotime($account->lastbillingtime) < strtotime($bstamp)) {
                $account->lastbillingtime = $bstamp;
                $account->save();
            }
            /* ���������� SMS ����������� */
            $this->sendSms($file, $account, $processedNumbers);
            /* ��������� ������ �� ������������� */
            $this->blockNumbers($file, $account, $processedNumbers);

            $this->markFile($file, self::FILE_STATUS_BILLPAGE);
        }
    }

    /**
     * ���������� ������� ������� ������� ���������
     * @param string $file ��� ����� ��� ���������
     * @return boolean
     */
    protected function updateBillPages($file)
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');
        $startTime = time();
        $detbalData = $this->getDetbalData($file);

        if ($detbalData === false) {
            \Yii::error("�� ������� �������� ������ �������� ����� �� ����� ���������� ������ {$file}.");
            return false;
        }

        $account = $this->getAccount($detbalData['an']);

        echo "Update billpages for account {$account->number}...";
        /* ���� ������� �������� ������ ���� ��������� ������ ������ */
        list($y, $m) = preg_split("/-/", $detbalData['sd']);
        $billpageDate = date("Y-m-d", mktime(0, 0, 0, intval($m) + 1, 0, intval($y)));
        foreach ($this->loadBillingData($file) as $data) {
            $number = $this->getNumber(substr($data['number'], -10), $account);

            $billpage = "<code>"
                    . preg_replace([
                        "/>/",
                        "/<(tp|ts|ss|bs|ds|\/tp)/",
                        "/</",
                        "/\r\n/"
                            ], [
                        "&gt;",
                        "\r\n<\\1",
                        "&lt;",
                        "\r\n<br>"
                            ], preg_replace('#<\?xml.*\?>#', '', $data['billpage'])
                    )
                    . "</code>";
            $number->setBillPage($billpage, $billpageDate);
            $this->updateBlockStatus($number, $data['billpage']);
        }
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return true;
    }

    /**
     * ���������� ���������� � ���������� ������
     * @param \common\models\Number $number �����
     * @param string $billPage XML, ���������� ������� �������� ������
     */
    protected function updateBlockStatus(\common\models\Number $number, $billPage)
    {
        $simpleXmlNode = simplexml_load_string($billPage);
        $curBlockStamp = 0;
        foreach ($simpleXmlNode->children() as $child) {
            if ($child->getName() == 'bs') {
                $data = $child->attributes();
                if (isset($data['n']) && $data['n'] == mb_convert_encoding('�������������� ����� �� ��������� �������� ��������������', 'utf-8', 'cp1251') && isset($data['sd']) && trim($data['sd']) != '' && strtotime($data['sd']) > $curBlockStamp
                ) {
                    $number->last_block_stamp = date("Y-m-d H:i:s", strtotime($data['sd']));
                    $number->is_blocked = 1;
                    if (isset($data['ed']) && trim($data['ed']) != "") {
                        $number->is_blocked = 0;
                        $number->last_block_stamp = "";
                    }
                    $curBlockStamp = strtotime($data['sd']);
                }
            }
        }
        /* ��������� ����� ������ ���� ��� ����� */
        if (count($number->getDirtyAttributes()) > 0) {
            $number->save();
        }
    }

    /**
     * ���������� �������
     * @param string $file ��� ����� ���������� ������
     * @param \common\models\MtsAccount $account ������� ����
     * @param array $processedNumbers ������������ ������
     * @return boolean
     */
    protected function blockNumbers($file, $account, $processedNumbers)
    {
        if ($processedNumbers === []) {
            return false;
        }

        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $startTime = time();
        echo "Block numbers with explicit block for account {$account->number}...\n";

        $notificationMessage = "������, ������� ���������� �������������:\r\n--------------------------\r\n";
        $sendNotification = false;
        $extra_block_enabled = intval(Constant::get('auto_block_extra_enable')) == 1;
        foreach ($this->loadBillingData($file) as $data) {
            $number = $this->getNumber(substr($data['number'], -10), $account);
            if (!in_array($number->id, $processedNumbers)) {
                continue;
            }

            $minus_currency = (int) Constant::get('block_currency');
            $minus_currency_desc = $minus_currency == 1 ? "$" : "���.";

            $number_bal = $number->getBalance("", $minus_currency);
            /* ����������� �����, ���� ���� �������������� ���������� �� ������ */
            if (intval($number->auto_block_extra) === 1 && doubleval($number_bal) <= doubleval($number->block_limit)) {
                echo "Number {$number->number} must be blocked\n";
                if ($extra_block_enabled === true) {
                    echo "try to block number\n";
                    $res = $number->block();
                    var_dump($res);
                } else {
                    echo "add number to notification message\n";
                    $sendNotification = true;
                    $notificationMessage.="{$number->number}\r\n";
                }
            }
        }
        if ($sendNotification === true) {
            /* ���������� ����������� �������������� */
            $email = [trim(Constant::get('notify_email'))];
            if (!empty($email)) {
                $mailer = \Yii::$app->mailer;
                /* @var $mailer \yii\swiftmailer\Mailer */
                $message = $mailer->compose();
                /* @var $message \yii\swiftmailer\Message */
                $message->setSubject(mb_convert_encoding('������, ���������� ����������', 'utf8', 'cp1251'));
                $message->getSwiftMessage()->setEncoder(new \Swift_Mime_ContentEncoder_Base64ContentEncoder());
                $message->setTextBody(mb_convert_encoding($notificationMessage, 'utf8', 'cp1251'));
                $message->setTo($email);

                try {
                    $message->send();
                } catch (\Exception $e) {
                    echo "Error when send mail message: {$e->getMessage()}\n";
                }
            }
        }
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return true;
    }

    /**
     * �������� SMS �� ������
     * @param string $file ��� ����� ���������� ������
     * @param \common\models\MtsAccount $account ������� ����
     * @param array $processedNumbers ������������ ������
     */
    protected function sendSms($file, $account, $processedNumbers = [])
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');
        $startTime = time();
        echo "Generating SMS for account {$account->number}...\n";
        foreach ($this->loadBillingData($file) as $data) {
            $number = $this->getNumber(substr($data['number'], -10), $account);
            if (!in_array($number->id, $processedNumbers)) {
                echo "{$number->number} billing not processed. ignore.\n";
                continue;
            }
            $send = false;
            /* ���������, ���� �� �������� SMS */
            switch (intval($number->sms_auto_send)) {
                case 0: {
                        $client = \common\models\Clients::find()
                                ->where(['id' => $number->getOwner(date('Y-m-d'))])
                                ->one();
                        if (!empty($client)) {
                            $send = intval($client->sms_auto_send) === 1;
                        }
                        break;
                    }
                case 1: {
                        $send = true;
                        break;
                    }
                case 2: {
                        $send = false;
                        break;
                    }
            }

            if (intval($account->sms_auto_send) == 0) {
                $send = false;
            }

            if ($send !== true) {
                /* �������� �� ���� */
                continue;
            }

            /* 	������ ���� ��������� ������ ������ � ��������� ��� */
            $minus_currency = (int) Constant::get('block_currency');
            $minus_currency_desc = $minus_currency == 1 ? "$" : "���.";

            $number_bal = $number->getBalance("", $minus_currency);
            $number_bal_frmt = round($number_bal, 2);

            $payment = $number->getPrepayment();
            $payment_currency = (int) Constant::get('prepayment_currency');
            $payment_currency_desc = $payment_currency == 1 ? "$" : "���.";
            $payment_days = (int) Constant::get('prepayment_days');

            /* ���������� ���������� ���������� ���� */
            $block_days = (int) Constant::get('block_days');

            if ($block_days == 0) {
                $days = 30;
            } else {
                $days = 1 * $block_days;
            }
            $credit_limit = $number->block_limit;

            $extra_minus = (double) Constant::get('block_balance');

            $month_minus = $number->getPeriodMinusForNoblock("", $minus_currency, 1);

            $sred_minus = $month_minus / $days;

            if ($sred_minus > 0) {
                $days_without_block = round(($number_bal - $extra_minus) / ($sred_minus), 0);
            } else {
                $days_without_block = 5;
            }
            /* ��������� ��������� */
            $message = "";
            if ($days_without_block > 4) { //	5 � ����� ����. ��������� �� ��������
                $message = "";
            } elseif ($days_without_block > 3) { //	4 ���
                $message = "��� ������� ������: {$number_bal_frmt} {$minus_currency_desc}. "
                        . "���� ����� ������ �� {$days_without_block} ���. "
                        . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}";
            } elseif ($days_without_block > 2) { //	3 ���
                $message = "��� ������� ������: {$number_bal_frmt} {$minus_currency_desc}. "
                        . "���� ����� ������ �� {$days_without_block} ���. "
                        . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                        . "���������� ��������� ������.";
            } elseif ($days_without_block > 1) { //	2 ���
                $message = "��� ������� ������: {$number_bal_frmt} {$minus_currency_desc}. "
                        . "���� ����� ������ �� {$days_without_block} ���. "
                        . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                        . "���������� ��������� ������.";
            } elseif ($days_without_block > 0) { //	1 ����
                $message = "��� ������� ������: {$number_bal_frmt} {$minus_currency_desc}. "
                        . "���� ����� ������ �� {$days_without_block} ����. "
                        . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                        . "���������� ��������� ������.";
            } elseif ($days_without_block <= 0) { //	������ �����. ��������� �����
                if ($number_bal > 0) {
                    $days_without_block = $days_without_block < 0 ? 0 : $days_without_block;
                    $message = "��� ������� ������: {$number_bal_frmt} {$minus_currency_desc}. "
                            . "���� ����� ������ �� {$days_without_block} ��. "
                            . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                            . "���������� ���������� ������.";
                } else {
                    if ($credit_limit < 0) {
                        if ($number_bal < $credit_limit) {
                            $message = "�������� ��������� �����. "
                                    . "��� ����� �������� ����������. "
                                    . "��� ������� ������: {$number_bal} {$minus_currency_desc}. "
                                    . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                                    . "���������� ���������� ������.";
                        } else {
                            $message = "��� ������� ������: {$number_bal} {$minus_currency_desc}. "
                                    . "��� ������������ ��������� ����� �� {$credit_limit}�. "
                                    . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                                    . "���������� ��������� ������.";
                        }
                    } else {
                        $message = "��� ����� �������� ����������. "
                                . "��� ������� ������: {$number_bal} {$minus_currency_desc}. "
                                . "������������� ���������� �� {$payment_days} ��.: {$payment} {$payment_currency_desc}. "
                                . "���������� ���������� ������.";
                    }
                }
            }

            /* 	����� ��������� SMS */
            if ($message != "") {
                if (intval($number_bal) < 0) {
                    /* ��� ������� � ������ ������� */
                    $priority = 3;
                } else {
                    $priority = 4;
                }

                if (\common\models\Sms::add($number->id, $message, $priority) == false) {
                    echo "Can't add SMS for {$number->number}\n";
                }
            }
        }
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        \Yii::endProfile(__METHOD__, 'detbal-worker');
    }

    /**
     * ���������� ������������� ��������������
     * @param string $file ��� ����� ���������� ������
     * @param \common\models\MtsAccount $account ������� ����
     * @param \common\models\Operation $operation ��������
     * @return boolean
     */
    protected function updateCommission($file, $account, $operation)
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $startTime = time();
        $detbalData = $this->getDetbalData($file);

        if ($detbalData === false) {
            \Yii::error("�� ������� �������� ������ �������� ����� �� ����� ���������� ������ {$file}.");
            return false;
        }

        echo "Update commission for account {$account->number}...";

        foreach ($this->loadBillingData($file) as $data) {
            $number = $this->getNumber(substr($data['number'], -10), $account);

            if ($number->getCommissionType($detbalData['ed']) == 2) {
                $number->resetbilling($detbalData['ed'], 5, 0);
                $commission = $number->getCommissionPayment($detbalData['ed'], 1, 1, 0);
                if ($commission > 0) {
                    $number->setBilling($detbalData['ed'], -1 * $commission, 5, 0, $operation->id);
                }
            }
        }
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return true;
    }

    /**
     * ���������� ����������� ���������� �������
     * @param string $file ��� ����� ���������� ������
     * @return boolean
     */
    protected function updateTalks($file)
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');
        $startTime = time();
        $detbalData = $this->getDetbalData($file);

        if ($detbalData === false) {
            \Yii::error("�� ������� �������� ������ �������� ����� �� ����� ���������� ������ {$file}.");
            return false;
        }

        echo "Update talks for account {$detbalData['an']}...";

        $talksData = $this->loadTalksData($file);
        \Yii::beginProfile('update talks in DB (run sql)', 'detbal-worker');

        $db = \Yii::$app->getDb();
        /* @var $db \yii\db\Connection */
        try {
            foreach ($talksData as $number => $data) {

                if (count($data['delete']) > 0) {
                    $sql = "DELETE FROM talks1 WHERE ";
                    $datesSql = "'" . implode("','", array_unique($data['delete'])) . "'";
                    $sql .= "number_id='{$this->getNumberId($number)}' AND date IN ({$datesSql})";
                    $db->createCommand($sql)->execute();
                }

                if (count($data['insert']) > 0) {
                    $period = $detbalData['sd'];
                    $insertSql = "INSERT INTO talks1 ("
                            . "number_id, period,service,date,time,type,number,ps_zone,vtk_zone,traf_type,duration,internet,summ)"
                            . " VALUES ";
                    $values = [];
                    foreach ($data['insert'] as $line) {
                        $sql = $insertSql
                                . "("
                                . "'{$this->getNumberId($number)}',"
                                . "'{$period}',"
                                . "'',"
                                . "'{$line['date']}',"
                                . "'{$line['time']}',"
                                . "{$line['type']},"
                                . "'{$line['number']}',"
                                . "'{$line['ps_zone']}',"
                                . "'{$line['vtk_zone']}',"
                                . "'{$line['traf_type']}',"
                                . "time_to_sec('{$line['duration']}'),"
                                . "{$line['internet']},"
                                . "{$line['summ']}"
                                . ")";
                        $db->createCommand($sql)->execute();
                    }
                }
            }
        } catch (\Exception $e) {
            echo "ERROR\n";
            echo $e->getMessage() . PHP_EOL . $e->getTraceAsString() . PHP_EOL;
            \Yii::error($e->getMessage(), 'detbal-worker');
        }

        \Yii::endProfile('update talks in DB (run sql)', 'detbal-worker');
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        return true;
    }

    /**
     * �������� ���������� � ��������� ������.
     * @param string  $file ��� ����� ���������� ������
     * @param boolean $cachedOnly ������ ���� ���������� ���� � ����
     * @return array ������ ���������� �� �������
     * @throws exceptions\CachedDataNotAvailableException
     * @throws exceptions\DetbalFileNotFoundException
     */
    protected function loadTalksData($file, $cachedOnly = false)
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');
        /* ��������� ����� �� ���� */
        $cached = \Yii::$app->getCache()->get([__METHOD__, $this->getSourceFileName($file)]);
        if ($cached !== false) {
            \Yii::endProfile(__METHOD__, 'detbal-worker');
            return $cached;
        }
        if ($cachedOnly) {
            throw new exceptions\CachedDataNotAvailableException("No cached talks data for file " . $this->getSourceFileName($file));
        }

        if (!file_exists($file)) {
            throw new exceptions\DetbalFileNotFoundException("Detbal file {$file} not found");
        }
        $talksData = [];
        /* ������ ���� � ������ �.�. ������ ������� 25Mb */
        $stream = new Stream\File($file, 1024 * 256);

        $parser = new Parser\StringWalker([
            'captureDepth' => 2,
        ]);

        $streamer = new XmlStringStreamer($parser, $stream);

        while ($node = $streamer->getNode()) {
            if ($node === false) {
                continue;
            }
            $simpleXmlNode = simplexml_load_string($node);
            if ($simpleXmlNode->getName() != 'ds') {
                continue;
            }
            $data = $simpleXmlNode->attributes();
            $children = $simpleXmlNode->children();

            if (!isset($data['n'])) {
                /* ��� ������ ������ */
                continue;
            }

            $number = substr($data['n'], -10);

            foreach ($children as $child) {
                if ($child->getName() != 'i') {
                    continue;
                }
                $childData = $child->attributes();
                $timestamp = $childData['d'];
                list($fmtDate, $time) = split(' ', $timestamp);
                $date = \Yii::$app->getFormatter()->format($fmtDate, ['date', 'Y-MM-dd']);
                $item = [
                    'date' => $date,
                    'time' => $time,
                    'type' => strpos($childData['n'], "<--") === FALSE ? 0 : 1,
                    'number' => mb_convert_encoding(str_replace("<--", "", $childData['n']), 'cp1251', 'utf-8'),
                    'ps_zone' => mb_convert_encoding($childData['zp'], 'cp1251', 'utf-8'),
                    'vtk_zone' => mb_convert_encoding($childData['zv'], 'cp1251', 'utf-8'),
                    'traf_type' => isset($childData['s']) ? mb_convert_encoding(preg_replace("#[\"']#", "", $childData['s']), 'cp1251', 'utf-8') : '����������',
                    'duration' => $this->buildTalksDuration($childData['du']),
                    'internet' => $this->buildTalksInternet($childData['du']),
                    'summ' => (double) str_replace(',', '.', $childData['c']),
                ];
                foreach ($item as $key => &$val) {
                    if (strpos($val, "'") !== false) {
                        $val = addcslashes($val, "'");
                    }
                }
                unset($val);
                $talksData["{$number}"]['insert'][] = $item;
                $talksData["{$number}"]['delete'][] = $date;
            }
        }
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        \Yii::$app->getCache()->set([__METHOD__, $this->getSourceFileName($file)], $talksData, 4 * 3600);
        return $talksData;
    }

    /**
     * ���������� ������������ ��������� �� ���������� �� ������ � ����������.
     * @param string $dup ������������ (����� ���� ��� �����, ��� � ����� �������)
     * @return string ������������ ��������� � ������� ������� ("00:00:00")
     */
    private function buildTalksDuration($dup)
    {
        if (strpos($dup, "Kb") === false) {
            $duration = preg_split("/:/", str_replace(" ", "", $dup));
            $dur_hour = 0;
            $dur_min = intval($duration[0]);
            if (count($duration) > 1) {
                $dur_sec = intval($duration[1]);
            } else {
                $dur_sec = "00";
            }

            if ($dur_min > 59) {
                //	�������� � ����
                $dur_hour = intval($dur_min / 60);
                $dur_min = $dur_min - $dur_hour * 60;
            }

            return "{$dur_hour}:{$dur_min}:{$dur_sec}";
        }
        return "00:00:00";
    }

    /**
     * ���������� ������ ������� �� ���������� �� ������ � ����������.
     * @param string $dup ����� ������� (����� ���� ��� �����, ��� � ����� �������)
     * @return string ������ � �������� �������������� ������ ������� � ����������
     */
    private function buildTalksInternet($dup)
    {
        if (strpos($dup, "Kb") === false) {
            return "0";
        }
        return preg_replace('#(Kb|,)#', '', $dup);
    }

    /**
     * ���������� ���������� � ����������� �� ������� ���������
     * @param string $file ��� ����� ���������� ������
     * @param \common\models\MtsAccount $account ������� ����
     * @param \common\models\Operation $operation ��������, � ������� ������� ����������
     * @return array ������ ������������ �������
     */
    protected function updateBilling($file, $account, $operation)
    {
        $processedNumbers = [];
        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $startTime = time();
        $detbalData = $this->getDetbalData($file);

        if ($detbalData === false) {
            \Yii::error("�� ������� �������� ������ �������� ����� �� ����� ���������� ������ {$file}.");
            return [];
        }

        echo "Update billing for account {$account->number}...";

        $rate = (double) Constant::get('rate', $detbalData['ed']);
        $insertBilling = [];

        foreach ($this->loadBillingData($file) as $data) {
            $number = $this->getNumber(substr($data['number'], -10), $account);

            /* ���� ���� �������� ������ ������ ��� ���� �������� �� FORIS
             * ����� ������ �������
             */
            if (strtotime($number->lastbillingtime) >= strtotime($detbalData['ed'] . ' 23:59:59')) {
                continue;
            }

            $minusByBase = $number->getPeriodMinus(
                    $detbalData['sd'], $detbalData['ed'], 0, false, true, true, [3, 6], false, $account->operator_id
            );

            $minusByFile = (double) $data['minus'];

            $delta_r = $minusByFile - $minusByBase;
            if (abs($delta_r) > 1.0E-7) {
                $delta_ue = $delta_r / $rate;
                $insertBilling[] = $this->buildInsertBillingRow($operation, $account, $number, $detbalData['ed'], $delta_r, $delta_ue);
            }
            $processedNumbers[] = $number->id;
        }
        $this->insertBilling($insertBilling);
        echo "OK (" . (time() - $startTime) . " sec.)\n";
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return $processedNumbers;
    }

    /**
     * �������� ���������� � ����������� �� ����� ���������� ������
     * @param string $file ��� ����� ���������� ������
     * @return array ������ ���������� � ����������� �������
     */
    private function loadBillingData($file)
    {
        if (isset($this->billingData[$file])) {
            return $this->billingData[$file];
        }
        /* ��������� ����� �� ���� */
        $cached = \Yii::$app->getCache()->get([__METHOD__, $this->getSourceFileName($file)]);
        if ($cached !== false) {
            return $this->billingData[$file] = $cached;
        }

        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $billingData = [];
        $stream = new Stream\File($file, 1024 * 1024);

        $parser = new Parser\UniqueNode([
            'uniqueNode' => 'tp'
        ]);

        $streamer = new XmlStringStreamer($parser, $stream);

        while ($node = $streamer->getNode()) {
            $simpleXmlNode = simplexml_load_string($node);
            $data = $simpleXmlNode->attributes();

            if (!isset($data['n']) || !isset($data['a']) || !isset($data['ed'])) {
                /* ��� ������ ������ */
                continue;
            }
            $billingData[] = [
                'number' => (string) $data['n'],
                'minus' => str_replace(',', '.', $data['a']),
                'billpage' => (string) $simpleXmlNode->asXML(),
            ];
        }
        $this->billingData[$file] = $billingData;
        \Yii::$app->getCache()->set([__METHOD__, $this->getSourceFileName($file)], $billingData, 4 * 3600);
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return $billingData;
    }

    private function buildInsertBillingRow($operation, $account, $number, $detbalEndDate, $delta_r, $delta_ue)
    {
        return [
            'oper_id' => !empty($operation) ? $operation->id : '',
            'operator_id' => $account->operator_id,
            'number_id' => $number->id,
            'mts_account_id' => $account->id,
            'client_id' => $number->getOwner($detbalEndDate),
            'date' => $detbalEndDate,
            'summ_r' => -1 * $delta_r / 1.18,
            'summ' => -1 * $delta_ue / 1.18,
            'operation_code' => 6,
        ];
    }

    /**
     * ���������� ���������� � �������� � ��
     * @param array $data
     * @return boolean
     */
    private function insertBilling($data)
    {
        if (!is_array($data)) {
            return false;
        }
        if (count($data) < 1) {
            return false;
        }
        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $sql = "LOCK TABLES billing_numbers_day WRITE;"
                . "INSERT INTO billing_numbers_day ("
                . "oper_id,operator_id,number_id,mts_account_id,client_id,date,time,summ,summ_r,operation_code"
                . ") VALUES ";
        foreach ($data as $row) {
            $sql.="("
                    . "'{$row['oper_id']}','{$row['operator_id']}','{$row['number_id']}',"
                    . "'{$row['mts_account_id']}','{$row['client_id']}','{$row['date']}',"
                    . "CURTIME(),{$row['summ']},{$row['summ_r']},{$row['operation_code']}"
                    . "),";
        }
        $sql = rtrim($sql, ',') . ';';
        $sql.="UNLOCK TABLES;";

        \Yii::$app->db->createCommand($sql)->execute();
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return true;
    }

    /**
     * ��������� ���������� ���������� ������
     * @param string $file ��� ����� ���������� ������
     * @return array ������ � ������ ���������� ������ � ������� �������� �����
     */
    private function getDetbalData($file)
    {
        \Yii::beginProfile(__METHOD__, 'detbal-worker');

        $stream = new Stream\File($file, 1024 * 1024);

        $parser = new Parser\UniqueNode([
            'uniqueNode' => 'b',
            'checkShortClosing' => true,
        ]);

        $streamer = new XmlStringStreamer($parser, $stream);

        $node = $streamer->getNode();
        if ($node) {
            $simpleXmlNode = simplexml_load_string($node);
            \Yii::endProfile(__METHOD__, 'detbal-worker');
            $data = $simpleXmlNode->attributes();
            return [
                'sd' => \Yii::$app->getFormatter()->format($data['ed'], ['date', 'Y-MM-01']),
                'ed' => \Yii::$app->getFormatter()->format($data['ed'], ['date', 'Y-MM-dd']),
                'an' => $data['an'],
            ];
        }
        \Yii::endProfile(__METHOD__, 'detbal-worker');
        return [];
    }

    /**
     * �������� ����� ��������
     * @param \common\models\MtsAccount $account
     * @return \common\models\Operation ����� ��������
     */
    private function createOperation(\common\models\MtsAccount $account)
    {
        $item = new \common\models\Operation;
        $item->date = date("Y-m-d");
        $item->time = date("H:i:s");
        $item->name = "��������� ���������� ������ �� �/� {$account->number}.";
        if ($item->save()) {
            return $item;
        }
        return null;
    }

    /**
     * ��������� �������� ����� �� ������
     * @param string $number ����� �������� �����
     * @return \common\models\MtsAccount ������� ����
     */
    private function getAccount($number)
    {
        $existingItem = \common\models\MtsAccount::find()
                ->where(['number' => $number])
                ->one();

        if (!empty($existingItem)) {
            return $existingItem;
        }
        $item = new \common\models\MtsAccount();
        $item->number = $number;
        $item->save();
        return $item;
    }

    /**
     * ��������� ������ �� ������ ��������
     * @param string $number ����� ��������
     * @param \common\models\MtsAccount $account ������� ����
     * @return \common\models\Number �����
     */
    private function getNumber($number, \common\models\MtsAccount $account)
    {
        if (isset($this->cacheNumber[$number])) {
            return $this->cacheNumber[$number];
        }


        $existingItem = \common\models\Number::find()
                ->where(['number' => $number])
                ->one();

        if (!empty($existingItem)) {
            $this->cacheNumber[$number] = $existingItem;
            return $existingItem;
        }
        $item = new \common\models\Number();
        $item->number = $number;
        $item->mts_account = str_pad((int) $account->id, 6, '0', STR_PAD_LEFT);
        $item->comment = "������ ��� �������� ���������� �� ���������� ������ (FORIS)";
        if (!$item->save()) {
            \Yii::error('Can not create new number "' . $number . '". '
                    . PHP_EOL
                    . print_r($item->getErrors(), 1), 'detbal-worker');
        }

        $this->cacheNumber[$number] = $item;

        return $item;
    }

    /**
     * ��������� ID ������ �� ������ ��������
     * @param string $number ����� ��������
     * @return string ID ������, ���� �� ������, null � ��������� ������
     */
    private function getNumberId($number)
    {
        if ($this->numberIds === []) {
            $query = new \yii\db\Query;
            $data = $query->select('id,number')
                    ->from('numbers')
                    ->all();
            $this->numberIds = \yii\helpers\ArrayHelper::map($data, 'number', 'id');
        }
        if (isset($this->numberIds["$number"])) {
            return $this->numberIds["$number"];
        }
        return null;
    }

    /**
     * ��������� ������ ��� ������ ��������� ������� �� �������
     * @param string $status ������
     * @return array ������ ��� ������
     */
    protected function getFiles($status = null)
    {
        $dirName = \Yii::$app->params['detbalsDir'];

        $filter = $status !== null ? ['*.' . $status] : ['*.xml'];
        return FileHelper::findFiles($dirName, [
                    'recursive' => false,
                    'only' => $filter,
        ]);
    }

    /**
     * ��������� ����� ����� ���������� ������ ��� ��������
     * @param string $file ��� ����� ���������� ������
     * @return string ��� ����� ��� ��������
     */
    protected function getSourceFileName($file)
    {
        return preg_replace('#^(.*?\.xml)(\..*)$#', '$1', $file);
    }

    /**
     * ���������� ����� ������� � ����� ����� ���������� ������.
     *
     * @param string $file ��� ����� ���������� ������
     * @param string $status ����� ������
     * @return string|boolean ����� ��� ����� � ������������ �� ��������, FALSE
     * ���� ������������� ���� �� �������
     */
    protected function markFile($file, $status)
    {
        $newName = $file . '.' . $status;
        if (rename($file, $newName)) {
            return $newName;
        }

        return false;
    }

}
