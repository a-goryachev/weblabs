/* 
 * The MIT License
 *
 * Copyright 2014 Alexander Goryachev <alexfoxlost@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


(function($, undefined) {
    "use strict";
    if ($.fn.YagiObjectSelector === undefined) {
        $.yagi = $.yagi || {}; // create the namespace
        /* set and get value */
        var oldVal = $.fn.val;
        $.fn.val = function(value) {
            var elem = this[0];

            if (!arguments.length) {
                if ($(elem).is(':yagi-YagiObjectSelector')) {
                    return $(elem).YagiObjectSelector("getValue");
                }
            } else {
                var args = arguments;
                return this.each(jQuery.proxy(function(i, v) {
                    if ($(v).is(':yagi-YagiObjectSelector')) {
                        if (value === null) {
                            $(v).YagiObjectSelector("setValue", null);
                        } else {
                            Yagi.loadObject($(v).YagiObjectSelector('option', 'objectType') + '_' + value, $.proxy(function(data) {
                                this.YagiObjectSelector("setValue", data);
                            }, $(v)));
                        }
                    } else {
                        return oldVal.apply($(v), args);
                    }
                }, this));

            }
            return oldVal.apply(this, arguments);
        };

        $.widget("yagi.YagiObjectSelector", $.yagi.YagiWidget, {
            version: "1.0.1",
            options: {
                objectType: null, //Вид поиска
                filter: [], //Фильтр выбираемых объектов [{attr:"filterAttr",value:filterData},{...}]
                listForm: 'select',
                width: 800, //Ширина окна поиска
                height: 550, //Высота окна поиска
                default_title: null, //Текстовое описание объекта (если есть объект)
                readonly: false,
                // callbacks
                beforeSelect: null, //Событие при начале выбора объекта
                select: null, //Событие. Выбор сделан
                cancel: null, //Событие. Отмена.
                clear: null, //Событие. Очистка поля.
                objectsList: null,
                dialog: null,
                title: null,
                select_done: false,
                currentSelection: [],
                currentValue: {id: null, defaultName: null, objectLink: null}
            },
            _create: function() {
                this._super('_create');
                var selector,
                        controlPane,
                        controlContent,
                        titlePane,
                        title,
                        controlButtons,
                        clearButton;

                /* Создадим селектор */
                selector = $('<div class="form-control"></div>')
                        .css('cursor', 'pointer');
                controlPane = $('<div class="container-fluid"></div>');
                controlContent = $('<div class="row"></div>');
                /* Создадим контейнер для описания */
                titlePane = $('<div class="col-xs-11"></div>');
                title = $('<span>' + this.property('options').default_title + '</span>')
                        .appendTo(titlePane)
                        ;
                this.property('options.title', title);
                controlButtons = $('<div class="col-xs-1"></div>');
                /* Создадим кнопку очистки поля */
                clearButton = $('<a class="btn btn-danger right btn-xs" title="Очистить"><i class="fa fa-times"></i></a>')
                        .appendTo(controlButtons)
                        ;
                controlContent
                        .append(titlePane)
                        .append(controlButtons)
                        ;
                controlPane.append(controlContent);

                this._on(clearButton, {
                    click: $.proxy(function(event) {
                        if (this.property('options').readonly === true) {
                            event.preventDefault();
                            return false;
                        }
                        this.setValue(null, event);
                    }, this)
                });

                this._on(titlePane, {
                    click: $.proxy(function(event) {
                        if (this.property('options').readonly === true) {
                            event.preventDefault();
                            return false;
                        }

                        if ($.isFunction(this.property('options').beforeSelect)) {
                            this._trigger("beforeSelect", event);
                        }
                        var beforeSelectEvent = $.Event('beforeSelect');
                        this.element.trigger(beforeSelectEvent);
                        if (!beforeSelectEvent.isDefaultPrevented()) {
                            this._showDialog.apply(this);
                        }
                    }, this)
                });

                controlPane.appendTo(selector);
                selector.appendTo(this.element);

            },
            _destroy: function() {

            },
            _reset: function() {
                this.property('options.select_done', false);
            },
            _cancel_select: function(event) {
                this.property('options.select_done', false);
                this._reset();
                this._trigger("cancel", event);
                return false;
            },
            _select: function(event) {
                var selection = this.property('options.objectsList').YagiObjectsEditor("getSelection");
                if (selection.length < 1) {
                    alert('Ничего не выбрано!');
                    return false;
                }
                if (selection.length > 1) {
                    alert('Выберите только один элемент!');
                    return false;
                }
                Yagi.loadObject(selection[0].objectLink, $.proxy(function(data) {
                    this.setValue(data);
                }, this));
            },
            getValue: function() {
                return this.property('options.currentValue');
            },
            setValue: function(value, event) {
                this.property('options.currentValue', value);
                this.property('options.select_done', true);
                if (value !== null) {
                    this.property('options.title').html(value.defaultName);
                    this._reset();
                    this._trigger("select", event, [value]);
                    $(this.element).trigger('select', [value]);
                    $(this.element).trigger('change', [value]);
                } else {
                    this.property('options.title').html('объект не выбран');
                    this._trigger("clear", event);
                    $(this.element).trigger('clear');
                    $(this.element).trigger('change', [null]);

                }
            },
            _showDialog: function(event, ui) {
                var currentFilter = this.property('filter') === null ? [] : this.property('filter');
                //Создадим диалог для осуществления поиска
                //Форма поиска
                this.property('options.objectsList', $('<div></div>')
                        .attr('id', $(this.element).attr('id') + '_list')
                        .YagiObjectsEditor({
                            objectType: this.property('options.objectType'),
                            filter: currentFilter,
                            listForm: this.property('options.listForm'),
                            multiselect: false,
                            clickRow: $.proxy(function(event, data) {
                                return false;
                            }, this),
                            dblclickRow: $.proxy(function(event, data) {
                                return false;
                            }, this)
                        }));
                this.property('options.dialog', $('<div></div>')
                        .prependTo($('body:first'))
                        .append(this.property('options.objectsList'))
                        .attr('id', $(this.element).attr('id') + '_selectorDialog')
                        .YagiModalDialog({
                            title: 'Выберите из списка',
                            sidebar: {
                                buttons: [
                                    $('<button type="button" class="btn btn-success">Выбрать</button>').click($.proxy(function(event) {
                                        if (this._select(event) !== false) {
                                            this.property('options.dialog').YagiModalDialog('hide');
                                        }
                                    }, this)),
                                    $('<button type="button" class="btn btn-danger">Закрыть</button>').click($.proxy(function(event) {
                                        this._cancel_select(event);
                                        this.property('options.dialog').YagiModalDialog('hide');
                                    }, this))
                                ]
                            },
                            onDestroy: $.proxy(function() {
                                this.property('options.dialog').remove();
                            }, this)
                        })
                        .YagiModalDialog('show')
                        );
            },
            addFilter: function(filter) {
                var currentFilter = this.property('filter') === null ? [] : this.property('filter');
                currentFilter.push(filter);
                this.property('filter', currentFilter);
            },
            clearFilter: function(attrName) {
                var currentFilter = this.property('filter') === null ? [] : this.property('filter');
                var newFilter = [];
                if (attrName) {
                    $.each(currentFilter, $.proxy(function(i, val) {
                        if (val.attr !== attrName) {
                            newFilter.push(val);
                        }
                    }, this));
                }
                this.property('filter', newFilter);
            }
        });
    }
}(jQuery));

